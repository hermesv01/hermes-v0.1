
# HERMES v0.1

Hardware:                 
	Flex Sensors
	Accelerometer
	Raspberry Pi 3 
	Raspberry Pi Zero
	ADC
	Resistors
	9 volt Batteries
	Push button

What is the project about? 
	Our project is an ASL translation glove, in which the user will be able 
	to sign basic sign language while wearing the glove and it will transmit 
	data to a raspberry pi to translate and display in letters that the user 
	had signed. 

What will it do? 
	The person wearing the glove will sign a certain letter of the alphabet 
	in sign language, and the sensors will send data back to the computer/code, 
	which will detect the letter signed and print the letter on a screen .
